#!/usr/bin/env python3

import os
import sys
import numpy as np
from epics import PV, caget, caput
import time
#import matplotlib.pyplot as plt

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32 172.16.110.89"

system_name  = sys.argv[1]
LLRFfilename = sys.argv[2]
KW           = int(float(sys.argv[3]))
DC_coupling  = float(sys.argv[4])
cable_number = sys.argv[5]
cable_att    = float(sys.argv[6])
filter_att   = float(sys.argv[7])

filepath = "../Measurements/"+ system_name + "/LLRF/" +LLRFfilename + "_raw.csv"
data = np.genfromtxt(filepath,delimiter=',')

Y = data[:,0]
C = data[:,1]


#plt.plot(C,Y,marker=11)


Y = np.reshape(Y,(len(Y),1))
C = np.reshape(C,(len(C),1))

Csq = np.square(C)

LLRFsycnt = np.arange(0,1.0005,0.0005)
LLRFsycnt = np.reshape(LLRFsycnt,(len(LLRFsycnt),1))

del_Y   = np.diff(Y,axis=0)
del_Csq = np.diff(Csq,axis=0)
del_C   = np.diff(C,axis=0)

A = np.hstack((del_Csq,del_C))
coeff = np.matmul( np.linalg.inv(  np.matmul( np.transpose(A),A ))  , np.matmul( np.transpose(A), del_Y) )

noise = Y - np.matmul( np.hstack((Csq,C)), coeff)
eps = np.mean(noise)

Yest = np.matmul( np.hstack((np.square(LLRFsycnt),LLRFsycnt)), coeff) + eps

#plt.plot(LLRFsycnt,Yest)
#plt.xlabel('Raw LLRF ADC')
#plt.ylabel('Power (EGU)')
#plt.savefig("../Measurements/"+ system_name + "/LLRF/" + LLRFfilename + '.png')
#plt.close()

llrffile = open("../Measurements/"+ system_name + "/LLRF/" + LLRFfilename + ".csv","w")
if KW == 1:
    llrffile.write("# KWatt" + ',' + " raw" +'\n')
else:
    llrffile.write("# Watt" + ',' + " raw" +'\n')

for cnt in range(len(LLRFsycnt)):
    llrffile.write(str(Yest[cnt][0]) + ',' + str(LLRFsycnt[cnt][0]) +'\n')
llrffile.write('# \n')
llrffile.write('# \n')
llrffile.write('# \n')
llrffile.write('# \n')
llrffile.write('# -----------------------------------------------------------\n')
llrffile.write("# Wave-guide Directional coupler attenuation is " + str(DC_coupling) + "dB" +'\n')
llrffile.write("# Cable number from Wave-guide Directional coupler to patch panel  is " + cable_number +'\n')
llrffile.write("# Cable attenuation is " + str(cable_att) + "dB" +'\n')
llrffile.write("# Filter and directional coupler attenuation is " + str(filter_att) + "dB" +'\n')
llrffile.write('# -----------------------------------------------------------\n')
llrffile.close()
