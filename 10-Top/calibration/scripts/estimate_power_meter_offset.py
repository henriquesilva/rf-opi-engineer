#!/usr/bin/env python3

import os
import sys
import numpy as np
from pvaccess import Channel
import time

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32"

number_meas               = 20
number_calibration_points = 20

power_lvl_start = float(sys.argv[1]) #dBm
power_lvl_end   = float(sys.argv[2]) #dBm
siggen          = sys.argv[3]
powmet          = sys.argv[4]
#print(siggen)

PWMTmeas  = np.zeros(number_calibration_points)
siggenset = np.zeros(number_calibration_points)

# Set signal generator power level
siggen_powerlevel = Channel(siggen + "GeneralPPwr-SP")
siggen_powerlevel.setTimeout(10.0)
siggen_powerlevel.put(power_lvl_start)

time.sleep(0.2)

# Switch signal generator RF ON
rf_on = Channel(siggen + "GeneralRF-Sel")
rf_on.put(1)
time.sleep(0.2)

# Convert from dBm to Watts
power_lvl_start = np.power(10,((power_lvl_start - 30)/10))
# Convert from dBm to Watts
power_lvl_end   = np.power(10,((power_lvl_end - 30)/10))
# Calculate step in Watts
power_step      = (power_lvl_end-power_lvl_start)/(number_calibration_points-1)

rf_RB = Channel(siggen + "GeneralRFLvl-RB")
for cnt in range(number_calibration_points):
    
    # Set Signal generator amplitude
    # Compute level to set in Watts
    power_lvl_level = power_lvl_start + (cnt*power_step)
    # Convert from Watts to dBm
    power_lvl_level = 10*np.log10(power_lvl_level) + 30

    # Set signal generator power level
    siggen_powerlevel.put(power_lvl_level)
    time.sleep(0.2)
    # Read back set value from signal generator
    power_lvl_levelRB = rf_RB.get().getDouble()
    time.sleep(0.2)

    statistical_meas = np.zeros(number_meas)
    power_scan = Channel(powmet + "Value_DBM.SCAN")
    pm_reading  = Channel(powmet + "Value_DBM")
    for meas_cnt in range(number_meas):
        # Read Power meter
        # Send trigger ON to Power meter

        power_scan.put(9)
        time.sleep(0.5)
        # Send trigger OFF Power meter
        power_scan.put(0)
        time.sleep(0.5)
        # Read Power meter reading
        rb = pm_reading.get().getDouble()
        time.sleep(0.5)
        # Save in measurement
        statistical_meas[meas_cnt]  = rb

    siggenset[cnt] = power_lvl_levelRB
    PWMTmeas[cnt]  = np.mean(statistical_meas)
    # Convert from dBm to Watts
    power_lvl_level = np.power(10,((power_lvl_level - 30)/10))

# Switch OFF signal generator
rf_on.put(0)
attenuation = np.mean(siggenset - PWMTmeas)
print(attenuation)
